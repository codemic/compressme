from django.apps import AppConfig


class CompressmeConfig(AppConfig):
    name = 'compressme'
