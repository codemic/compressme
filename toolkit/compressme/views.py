from random import randint
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import default_storage
from django.conf import settings
# Create your views here.

@csrf_exempt
def index(request):
    from PIL import Image
    from io import BytesIO
    from base64 import encodebytes
    import json
    import os
    import random
    from  datetime import datetime, timedelta
    if request.method == "POST":
        
        if len(request.FILES) > 0:
            if request.FILES['file']:
                img = Image.open(request.FILES['file'])
                temp = BytesIO()
                img = img.convert('RGB')
                filename = "Compressed_"+str(random.randint(1,884694684))+request.FILES['file'].name
                
                q = int(request.POST.get('quality', 20))
                if q < 20:
                    q = 20
                if q > 95:
                    q = 95
                img.save(os.path.join(settings.MEDIA_ROOT, filename), "JPEG", optimize = True, quality = q)
    
                print(os.path.join(settings.MEDIA_ROOT, filename))
                # encoded_img = encodebytes(temp.getvalue()).decode('utf-8') 
                # response = HttpResponse(temp.getvalue(), content_type='image/jpeg')
                # response['Content-Disposition'] = 'attachment; filename='+request.FILES['file'].name.split(".")[0]+'_compressed.jpeg'
                return HttpResponse(json.dumps({'status': "success", 'image': filename, 'size': os.stat(os.path.join(settings.MEDIA_ROOT, filename)).st_size}), content_type="application/json")

    context = {
        'time': datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
    }
    return render(request, 'compressme/index.html', context)
