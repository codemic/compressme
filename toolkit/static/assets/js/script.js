$(function(){

    
    function startTime() {
        const today = new Date($("#time").html());  
        var t = new Date(today.getTime() + 1000)  
        document.getElementById('time').innerHTML = t;
        setTimeout(startTime, 1000);
    } 
    startTime();

    var ul = $('#upload ul');

    $('#drop a').click(function(){
        // Simulate a click on the file input button
        // to show the file browser dialog
        $(this).parent().find('input').click();
    });

    // Initialize the jQuery File Upload plugin
    $('#upload').fileupload({

        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),
        
        formData: {
            quality: $("#quality").val()
        },
        // processQueue: [ {
        // action: 'validate',
        // // Always trigger this action,
        // // even if the previous action was rejected: 
        // always: true,
        // acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        // }],
        // // The regular expression for allowed file types, matches
        // // against either file type or file name:
        // acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        // // The maximum allowed file size in bytes:
        // limitMultiFileUploadSize: 10000000, // 10 MB
        // // The minimum allowed file size in bytes:
        // minFileSize: undefined, // No minimal file size
        // // The limit of files to be uploaded:
        // limitMultiFileUploads: 10,
        

        // // Function returning the current number of files,
        // // has to be overriden for maxNumberOfFiles validation:
        // getNumberOfFiles: $.noop,

        // // Error and info messages:
        // messages: {
        //     maxNumberOfFiles: 'Maximum number of files exceeded',
        //     acceptFileTypes: 'File type not allowed',
        //     maxFileSize: 'File is too large',
        //     minFileSize: 'File is too small'
        // },
        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {
            $("#drop").css({'background-color': "#2E3134"});
            var tpl = $('<li class="working"><div><input type="text" value="0" data-width="48" data-height="48"'+
                ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p></div></li>');

            // Append the file name and file size
            var regex = /(\.|\/)(gif|jpe?g|png)$/i;
            
            if (regex.test(data.files[0].name)){
                if (data.files[0].size < 10000000) {
                    $("#message").html("");
                    tpl.find('p').text(data.files[0].name)
                                .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

                    // Add the HTML to the UL element
                    data.context = tpl.prependTo(ul);

                    // Initialize the knob plugin
                    tpl.find('input').knob();

                    // Listen for clicks on the cancel icon
                    tpl.find('span').click(function(){

                        if(tpl.hasClass('working')){
                            jqXHR.abort();
                        }

                        tpl.fadeOut(function(){
                            tpl.remove();
                        });

                    });

                    // Automatically upload the file once it is added to the queue
                    data.formData = {
                        quality: $("#quality").val()
                    };
                    var jqXHR = data.submit();
                }
                else {
                    
                    $("#message").html("File size too large.");
                   
                }
            } else {                        
                
                $("#message").html("Only image files allowed");
                 
                
            }
        },

        progress: function(e, data){

            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if(progress == 100){
                data.context.removeClass('working');
            }
        },

        fail:function(e, data){
            // Something has gone wrong!
            data.context.addClass('error');
            console.log(data);
        },
        done: function (e, data) {
            console.log(data);
            var tmp = '<div><a class="download" download="'+data.result.image+'" target="_blank" href="/media/'+data.result.image+'"><img src="/static/assets/img/save.png" height=35 width=35><i>'+formatFileSize(data.result.size)+'</i></a></div>';
            

            // Add the HTML to the UL element
            console.log(data.context);
            data.context.append(tmp);

        }

    });


    // Prevent the default action when a file is dropped on the window
    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    // Helper function that formats the file sizes
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }

});